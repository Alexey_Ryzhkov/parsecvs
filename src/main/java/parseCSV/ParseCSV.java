package parseCSV;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class ParseCSV {

    public static boolean isNumeric(String str) {
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }

    public static void main(String[] args) throws Exception {
        try {
            String pathFile = "test.csv"; //Путь к файлу
            CSVReader reader = new CSVReader(new FileReader(pathFile));
            AtomicInteger count = new AtomicInteger(0); //Счетчик ограничений
            System.out.println("Введите дату в формате MM.DD.YYYY");
            Scanner scanner = new Scanner(System.in);
            String[] tempDateArr = scanner.next().split("\\.");
            String strDate = "";
            if (tempDateArr.length == 3) {  //Проверка корректности введеной даты
                strDate = tempDateArr[2] + tempDateArr[0] + tempDateArr[1];
            } else {
                throw new Exception("Дата введена в некоректном формате");
            }
            if (isNumeric(strDate) && strDate.length() == 8) {  //Проверка на то, возможно ли преобразовать дату к дате в CSV
                System.out.println("Указанная дата преобразована в " + strDate);
                int date = Integer.parseInt(strDate);
                List<String[]> allRows = reader.readAll();
                allRows.forEach(row -> {
                    if (isNumeric(row[11]) && isNumeric(row[10])) {
                        Integer startDate = Integer.parseInt(row[10]);
                        Integer endDate = Integer.parseInt(row[11]);
                        if (date < startDate || endDate < date) return;//Проверка на то, попадает ли введеная дата в промежуток каких либо работ
                        count.incrementAndGet();
                    }
                });
                System.out.println("Количество открытых ограниченй " + count);
            }
            else {
                throw new Exception("Дата введена в некоректном формате");
            }
        } catch (Exception exp) {
            System.out.println(exp.getMessage());
        }
    }
}
